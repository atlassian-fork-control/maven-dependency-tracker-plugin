package com.atlassian.maven.plugins

import org.apache.maven.artifact.Artifact

class DependencyReportLine implements Comparable
{
    String groupId, artifactId, version, type, classifier, scope, artifactChecksum, pomChecksum
    List<String> usedBy

    String toString()
    {
        return toString(true, true)
    }

    String toString(boolean showArtifactChecksum, boolean showPomChecksum)
    {
        def artifactChecksum = showArtifactChecksum ? "|${this.artifactChecksum}" : ''
        def pomChecksum = showPomChecksum ? "|${this.pomChecksum}" : ''
        
        return "$groupId:$artifactId:$version:$type:$classifier:$scope|${usedBy.join(',')}$artifactChecksum$pomChecksum"
    }

    /**
     * Used for sorting dependency report lines
     */
    public int compareTo(Object line)
    {
        if (!line || !(line instanceof DependencyReportLine)) return -1
        int g = groupId.compareTo(line.groupId)
        if (g) return g
        int a = artifactId.compareTo(line.artifactId)
        if (a) return a
        int v = version.compareTo(line.version)
        if (v) return v
        int t = type.compareTo(line.type)
        if (t) return t
        int c = classifier.compareTo(line.classifier)
        if (c) return c
        int s = scope.compareTo(line.scope)
        return s
    }

    public boolean equals(Object obj)
    {
        toString().equals(obj.toString())
    }

    public int hashCode()
    {
        toString().hashCode()
    }

    static DependencyReportLine from(Artifact artifact, Closure checksum)
    {
        new DependencyReportLine(
                groupId: artifact.groupId,
                artifactId: artifact.artifactId,
                version: artifact.baseVersion,
                type: artifact.type,
                classifier: artifact.classifier != null ? artifact.classifier : '-', // if it doesn't have a classifier, then use -
                scope: artifact.scope,
                artifactChecksum: artifact.snapshot ? 'SNAPSHOT' : checksum(artifact.file),
                pomChecksum: artifact.snapshot ? 'SNAPSHOT' : checksum(pom(artifact)),
                // getting the trail, removing the current project as well as the artifact we're talking about
                usedBy: removeFirstAndLast(artifact.dependencyTrail))
    }

    /**
     * Gets the pom file given an artifact, {@code null} if it doesn't exists
     */
    private static File pom(Artifact artifact)
    {
        if (!artifact.file) return null
        if (artifact.file.path.endsWith('.pom')) return artifact.file

        // replace the existing extension with .pom
        final def path = artifact.file.absolutePath
        new File("${path.substring(0, path.lastIndexOf('.'))}.pom")
    }

    static DependencyReportLine from(String line)
    {
        def lineArray = line.split('\\|') // it is a regexp, remember?
        assert lineArray.length == 4, "Should have an array of 3 elements artifact|trail|artifactChecksum|pomChecksum, got $lineArray.length"

        def artifactArray = lineArray[0].split(':')
        assert artifactArray.length == 6, "Should have an array of 6 elements groupId:artifactId:version:type:classifier:scope, got $artifactArray.length"

        new DependencyReportLine(
                groupId: artifactArray[0],
                artifactId: artifactArray[1],
                version: artifactArray[2],
                type: artifactArray[3],
                classifier: artifactArray[4],
                scope: artifactArray[5],
                artifactChecksum: lineArray[2],
                pomChecksum: lineArray[3],
                usedBy: lineArray[1].tokenize(','))
    }

    private static List removeFirstAndLast(List list)
    {
        list.tail().reverse().tail().reverse()
    }
}