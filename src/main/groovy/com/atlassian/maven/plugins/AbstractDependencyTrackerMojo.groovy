package com.atlassian.maven.plugins

import org.apache.commons.collections.CollectionUtils
import org.apache.commons.io.FileUtils
import org.apache.maven.artifact.Artifact
import org.apache.maven.artifact.resolver.filter.ScopeArtifactFilter
import org.apache.maven.project.MavenProject
import org.codehaus.groovy.maven.mojo.GroovyMojo

abstract class AbstractDependencyTrackerMojo extends GroovyMojo
{
    /**
     * The Maven Project
     *
     * @parameter expression="${project}"
     * @readonly
     */
    MavenProject project

    /**
     * The file name to use for reporting
     * @parameter expression="${dependency-tracker.report}" default-value="dependencies"
     * @required
     */
    String reportFileName = 'dependencies'

    /**
     * The name of the checksum algorithm to be used. Typically md5, sha1, etc.
     * @parameter expression="${dependency-tracker.checksum}" default-value="sha1"
     * @required
     */
    String checkSum = 'sha1'

    /**
     * Whether to include this project's direct dependencies in any reports or validation.
     *
     * @parameter expression="${dependency-tracker.includeDirectDependencies}" default-value="true"
     * @required
     */
    boolean includeDirectDependencies

    /**
     * Include only artifacts with this scope in the report. Defaults to "compile".
     *
     * @parameter expression="${dependency-tracker.includeScope}" default-value="compile"
     * @required
     *
     * @see ScopeArtifactFilter
     */
    String includeScope

    SortedSet<DependencyReportLine> reportLines()
    {
        def scope = new ScopeArtifactFilter(includeScope)
        log.debug "Including artifacts with scope '${includeScope}'"

        def dependencyStrings = project.dependencyArtifacts.collect({it.toString()}) as Set
        def reportArtifacts = project.artifacts.findAll { Artifact a ->
            (includeDirectDependencies || !dependencyStrings.contains(a.toString())) && scope.include(a)
        }

        final SortedSet<DependencyReportLine> report = new TreeSet()
        reportArtifacts.each {Artifact a -> report.add(DependencyReportLine.from(a, this.&calculateCheckSum)) }

        return report
    }

    protected File actualReportDir

    def calculateCheckSum = {File file ->
        if (!file?.exists())
        {
            log.info "File for $file doesn't exists"
            return '?'
        }
        def checkSumName = "${file.absolutePath}.checksum"
        ant.checksum(file: file.absolutePath, property: checkSumName, algorithm: checkSum)
        "{$checkSum}${ant.project.properties[checkSumName]}"
    }

    boolean diffReports(boolean validateArtifactChecksums, boolean validatePomChecksums) {
        final String reportDirName = "${project.basedir}/${reportFileName}"
        final String actualReportDirName = "${reportDirName}.actual"

        actualReportDir = new File(actualReportDirName)
        if (actualReportDir.exists()) {
            FileUtils.forceDelete(actualReportDir)
        }

        final File reportDir = new File(reportDirName)
        if (!reportDir?.exists())
        {
            log.warn("$reportDir.absolutePath doesn't exists, won't validate dependencies")
            return
        }

        def fileReport = new ArrayList()
        reportDir.eachFileRecurse { file ->
            fileReport.addAll(file.readLines().collect { DependencyReportLine.from(it) })
        }
        def runtimeReport = reportLines()

        // used to build a map of (compare string, display string)
        def mapByCompareString = { Map memo, DependencyReportLine entry ->
            memo[entry.toString(validateArtifactChecksums, validatePomChecksums)] = entry.toString()
            return memo
        }

        def fileReportMap = fileReport.inject([:], mapByCompareString)
        def runtimeReportMap = runtimeReport.inject([:], mapByCompareString)

        def newDependencies = CollectionUtils.subtract(runtimeReportMap.keySet(), fileReportMap.keySet())
        def removedDependencies = CollectionUtils.subtract(fileReportMap.keySet(), runtimeReportMap.keySet())

        if (!newDependencies && !removedDependencies)
        {
            log.debug "Calculated dependencies are the same as in the report"
            return
        }

        if (newDependencies)
        {
            log.error "The following dependencies are not present in the report:"
            newDependencies.each { log.error "\t${runtimeReportMap[it]}" }
        }

        if (removedDependencies)
        {
            log.error "The following dependencies are present in the report but no longer calculated:"
            removedDependencies.each { log.error "\t${fileReportMap[it]}" }
        }

        actualReportDir = writeReportToDirectory(actualReportDirName, runtimeReport)
    }

    File writeReportToDirectory(String dirName, Set<DependencyReportLine> report)
    {
        final File dir = new File(dirName)
        dir.mkdir()

        SortedMap<String, SortedSet<DependencyReportLine>> reportFiles = new TreeMap()
        report.each { line ->
            if (line.usedBy.isEmpty()) {
                throw new IllegalStateException("No dependency trail found for ${line} - please ensure there are no artifacts that have been relocated to it (they will be logged as warnings)")
            } else {
                final String name = line.usedBy.get(0)
                reportFiles.putIfAbsent(name, new TreeSet<DependencyReportLine>())
                reportFiles.get(name).add(line)
            }
        }

        reportFiles.each { name, contents ->
            new File(dir.absolutePath, name).withWriter { writer -> contents.each { writer << "$it\n" } }
        }

        return dir
    }
}
