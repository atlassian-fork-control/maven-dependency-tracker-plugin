package com.atlassian.maven.plugins

/**
 * @goal checksum
 * @requiresProject false
 */
class CheckSumMojo extends AbstractDependencyTrackerMojo
{
    /**
     * The path to the file to checksum
     * @parameter expression="${file}"
     * @required
     * @readonly
     */
    String file

    void execute()
    {
        log.info "Checksum for <$file> is:"
        log.info "${calculateCheckSum(new File(file))}"
    }
}
