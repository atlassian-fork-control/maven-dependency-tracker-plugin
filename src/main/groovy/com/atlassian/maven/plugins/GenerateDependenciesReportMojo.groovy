package com.atlassian.maven.plugins

/**
 *
 * @requiresDependencyResolution test
 * @goal report
 */
class GenerateDependenciesReportMojo extends AbstractDependencyTrackerMojo
{
    void execute()
    {
        diffReports(false, false)
    }
}
